import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Buka browser
WebUI.openBrowser(null)
//Masuk ke website demoaut.katalon.com
WebUI.navigateToUrl('http://demoaut.katalon.com/')
//Perintah untuk click button Appointment
WebUI.click(findTestObject('Page_CURA Healthcare Service/a_Make Appointment'))
//Isi username dengan John Doe
WebUI.setText(findTestObject('Page_CURA Healthcare Service/input_Username_username'), 'John Doe')
//Isi password dengan ThisIsNotAPassword
WebUI.setText(findTestObject('Page_CURA Healthcare Service/input_Password_password'), 'ThisIsNotAPassword')
//Click button login
WebUI.click(findTestObject('Page_CURA Healthcare Service/button_Login'))
//Perintah untuk memilih Seoul CURA Healthcare Center pada selected option
WebUI.selectOptionByValue(findTestObject('Page_CURA Healthcare Service/select_Tokyo CURA Healthcare Center                            Hongkong CURA Healthcare Center                            Seoul CURA Healthcare Center'),
	'Seoul CURA Healthcare Center', true)
//Click checkbox Apply for hospital readmission
WebUI.click(findTestObject('Page_CURA Healthcare Service/input_Apply for hospital readmission_hospital_readmission'))
//Memilih Healthcare program medicaid
WebUI.click(findTestObject('Page_CURA Healthcare Service/label_Medicaid'))
//isi tanggal 03/11/1992
WebUI.setText(findTestObject('Page_CURA Healthcare Service/input_Visit Date (Required)_visit_date'),'03/11/1992')
//isi text area dengan kalimat hai katalon
WebUI.setText(findTestObject('Page_CURA Healthcare Service/textarea_Comment_comment'),'Hai Katalon')
//click button book appointment
WebUI.click(findTestObject('Page_CURA Healthcare Service/button_Book Appointment'))
//click button homepage
WebUI.click(findTestObject('Page_CURA Healthcare Service/a_Go to Homepage'))
//close browser
WebUI.closeBrowser()